1. Navigate to index.html and learn the functionality of the page.

2. List all automation tests you can think of.

3. Create automation tests for the pages using C# and Selenium.

Note:
Every element on the pages has special attribute called "data-automation-id". Use it to find the elements when writing automation tests.