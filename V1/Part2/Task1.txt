1. Use SQL Server Management Studio and create new database 'AutomationInterview' for your local SQL Server.
2. Run the sql script from the file CreateTables.sql
3. Find out what tables created for the database.
4. Explain the structure of one table (columns, primary key)