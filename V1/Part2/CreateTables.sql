USE [AutomationInterview]
GO

/****** Object:  Table [dbo].[Employee]    Script Date: 3/11/2020 5:08:43 PM ******/
SET ANSI_NULLS ON
GO

SET QUOTED_IDENTIFIER ON
GO

CREATE TABLE [dbo].[Employee](
    [Id] int IDENTITY(1,1) PRIMARY KEY,
	[FirstName] [nvarchar](50) NOT NULL,
	[LastName] [nvarchar](50) NOT NULL,
	[Title] [nvarchar](50) NOT NULL,
	[Age] [int] NOT NULL
) ON [PRIMARY]
GO


