Create C# console application that outputs odd integer numbers from 1 to 20.
Numbers are displayed on the same line separated by space.

Note: Use "Range" method from Enumerable class defined in the "System.Linq" namespace and string.Join method.

Example
Output:
1 3 5 7 9 11 13 15 17 19